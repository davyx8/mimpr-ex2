function ex2()
mri1=load_untouch_nii('brainMRI1.nii');
mri2=load_untouch_nii('brainMRI2.nii');
mri3=load_untouch_nii('brainMRI3.nii');
mri2seg=load_untouch_nii('brainMRI2_seg.nii');
toSave=mri2seg;
mri2seg=mri2seg.img;
mri3seg=load_untouch_nii('brainMRI3_seg.nii');
mri1=mri1.img;
mri2=mri2.img;
mri3=mri3.img;
directions=['x', 'y' ,'z'];
% Q2.C find shift between mri1 and mri 
% parralerly find rot between mri1 and mri3
gradesShift=[];
shifts=[];
rotations=[];
gradesRotation=[]; 
tic
parfor i=1:3
    [shift g]=Find_Shift(mri1,mri2,1,i);
    gradesShift=[gradesShift g];
    shifts=[shifts shift]
end

bestShift=max(shifts)
direction=directions(find(shifts==bestShift));
shiftMat=zeros(3,1);
shiftMat(find(shifts==bestShift))=-bestShift;
mri2segShifted=imtranslate(mri2seg,shiftMat);

%registrating the actual image
result=zeros([size(mri1) 3]);
for i=1:size(mri1,3)
result(:,:,i,:)=imfuse(mri1(:,:,i),mri2segShifted(:,:,i));
end

toSave.img=result;
save_untouch_nii(toSave,'MRI2SegmentedAndShifted');



display('The direction is :'); 
direction
display('the shift is:');
bestShift
toc



 parfor i=1:3
     [rot g2] =Find_Rotation(mri1,mri3,1,i);
     gradesRotation=[gradesRotation g2];
     rotations=[rotations rot];
    
 end
bestRot=max(rotations)
direction2=directions(find(rotations==bestRot))

mri3segShifted=imrotate(mri3seg.img,-bestRot);

%registrating the actual image
result2=zeros([size(mri1) 3]);
for i=1:size(mri1,3)
result2(:,:,i,:)=imfuse(mri1(:,:,i),mri3segShifted(:,:,i));
end

toSave.img=result2;
save_untouch_nii(toSave,'MRI3SegmentedAndRotated');



toc
% sprintf('the rotation is %d pixels', bestRotation);

end