function [shift g]= Find_Shift(image1,image2,methodNum,dimNum)
%tic
[x y z]=size(image1)
dims=[y x z];
if methodNum == 1
    func = @Normalized_cross_correlation;
else
    func = @Mutual_Information;
end
padTemp = zeros(1,3);
padTemp(dimNum) = dims(dimNum);
paddington=[padTemp(2) padTemp(1) padTemp(3)];
image1Pad = padarray(image1,paddington);
image2Pad = padarray(image2,paddington);
paddington = paddington./sum(paddington)
grades=[0];
idx=[0];
parfor i=-dims(dimNum):1:dims(dimNum)
    shiftedImage1 = circshift(image1Pad, paddington*i);
    grade = func(image2Pad, shiftedImage1);
    grades=[grades grade];
    idx=[idx i];
end
plot(grades)
g=grades;
maxIdx=find(grades==max(grades))
shift=idx(maxIdx)
%toc
end