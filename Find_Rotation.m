function [rot grade] = Find_Rotation(image1,image2,methodNum,dimNum)
%tic
if methodNum == 1
    func = @Normalized_cross_correlation;
else
    func = @Mutual_Information;
end

if dimNum == 1
    permuteMat=[1 3 2];
    display('x');
elseif dimNum == 2
    display('y');
    permuteMat=[3 2 1];
else
    display('z');
    permuteMat=[1 2 3];
end
image1 = permute(image1,permuteMat);
image2 = permute(image2,permuteMat);
grades=[0];
idx=[0];
parfor i=-179:1:180
    imRotated = imrotate(image1,i,'crop');
    grade = func(image2, imRotated );
    grades=[grades grade];
    idx=[idx i];
end
plot(grades);
grade=max(grades);
maxIdx=find(grades==grade);
rot=idx(maxIdx);
%toc
end




