function [ M ] = Mutual_Information( image1,image2 )
im1 = double(image1);
im2 = double(image2);
nomalized1 = im1 - min(im1(:)) +1; 
nomalized2 = im2 - min(im2(:)) +1;
joint(:,1) = nomalized1(:);
joint(:,2) = nomalized2(:);
jointHistogram = accumarray(joint+1, 1); 
jointHistogramNor   malized = jointHistogram./sum(jointHistogram(:)); 
im2_marg=sum(jointHistogramNormalized,1); 
im1_marg=sum(jointHistogramNormalized,2);
Histogramy = - sum(im2_marg.*log2(im2_marg + (im2_marg == 0))); 
Histogramx = - sum(im1_marg.*log2(im1_marg + (im1_marg == 0))); 
arg_xy2 = jointHistogramNormalized.*(log2(hn+(jointHistogramNormalized==0)));
Histogramxy = sum(-arg_xy2(:)); 
M = Histogramx + Histogramy - Histogramxy; 
end