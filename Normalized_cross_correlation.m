function grade= Normalized_cross_correlation(image1,image2)
% im1 = load_untouch_nii(image1);
% im2 = load_untouch_nii(image2);
im1=im2double(image1);
im2=im2double(image2); 
mean1 = mean(im1(:));
std1 = std(im1(:));
dist1 = (im1 - mean1) ./ std1;

mean2 = mean(im2(:));
std2 = std(im2(:));

dist2 = (im2 - mean2) ./ std2;

res = dist1.*dist2;

grade = mean(res(:));

end
